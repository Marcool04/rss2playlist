#!/usr/bin/env bash

set -euo pipefail

for command in "curl" "xsltproc" "xmlstarlet"; do
  if ! which "$command" >/dev/null; then
    echo "The $command utility is needed, please install it and try again."
    exit 10
  fi
done

usage() {
  echo "Usage: $(basename "$0") [-c cache_folder] [-o output_folder] <URL> OR </path/to/file_with_urls.txt>" 1>&2
  echo "   -h | -?         -> display this help message" 1>&2
  echo "   -c              -> cache folder for rss files (default: /tmp/rss2playlist)" 1>&2
  echo "   -o              -> output folder to generate playlist(s) in (default: ./)" 1>&2
  echo "   URL             -> URL of an rss feed (REQUIRED, unless file_with_urls)" 1>&2
  echo "   file_with_urls  -> a file containing URLs of rss feeds (REQUIRED, unless URL)" 1>&2
}

cache_folder="/tmp/rss2playlist"
output_folder="$(pwd)"

while getopts "c:o:h?" opt; do
  case $opt in
    c) cache_folder="$OPTARG"
    ;;
    o) output_folder="$OPTARG"
    ;;
    h|?) usage
       exit 0
    ;;
    *) echo "Failed to understand options."
       usage
       exit 2
    ;;
  esac
done
shift $((OPTIND -1))

if [[ "$#" != 1 ]]; then
  echo "Wrong positional argument configuration (exactly ONE of URL or file_with_urls required)"
  usage
  exit 2
fi
positional_arg="$1"

declare -a links
if curl --silent "$positional_arg" >/dev/null; then
  echo "A request to $positional_arg was successful, treating as a URL"
  links=("$positional_arg")
else
  echo "Failed to get $positional_arg, treating as file path"
  if [[ -e "$positional_arg" ]]; then
    echo "Found file $positional_arg"
    mapfile -t links  < <(grep -v "^ *#" < "$positional_arg")
  fi
fi

mkdir -p "$cache_folder"

# This is taken and adapted from https://github.com/buzink/podcast2playlist
xsl="$(cat << EOF
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="text" indent="no" encoding="UTF-8" omit-xml-declaration="yes"/>
<xsl:template match="rss/channel">
<xsl:text>#EXTM3U</xsl:text>
<xsl:apply-templates select="item" />
</xsl:template>
<xsl:template match="item">
<xsl:variable name="item_enclosure" select="enclosure/@url"/>
<xsl:if test="string-length(enclosure/@url) > 10">
#EXTINF:0,<xsl:value-of select="title" /> - <xsl:value-of select="pubDate" />
<xsl:value-of select="'&#10;'"/>
<xsl:value-of select="enclosure/@url" />
</xsl:if>
</xsl:template>
</xsl:stylesheet>
EOF
)"

error_count=0
for feed_url in "${links[@]}"; do
  if ! curl --silent "$feed_url" >/dev/null; then
    echo "Failed to get $feed_url, skipping…"
    continue
  fi
  # xmlstarlet docs at: http://xmlstar.sourceforge.net/doc/UG/xmlstarlet-ug.html#idm47077139653312
  title="$(
    curl --silent --location "$feed_url" | \
    xmlstarlet select --template --match '//channel/title[1]' --value-of .
  )"
  if [[ -z "$title" ]]; then
    echo "Failed to parse $feed_url scrape to get a title, skipping…"
    continue
  fi
  echo "Have title: $title"
  rss_file="${title}.rss"
  playlist_file="${title}.m3u"
  echo "Downloading or updating $feed_url to ${cache_folder}/${rss_file}"
  if ! curl --silent \
    --location \
    --output "${cache_folder}/${rss_file}" \
    --time-cond "${cache_folder}/${rss_file}" \
    "$feed_url"
  then
    echo "Failed to get feed $feed_url"
    ((error_count++))
    continue
  fi
  echo "Converting $rss_file to $playlist_file"
  if ! xsltproc -o "${output_folder}/${playlist_file}" \
    <(echo "$xsl") "${cache_folder}/${rss_file}"
  then
    echo "Failed to convert $rss_file to m3u using xsltproc"
    ((error_count++))
    continue
  fi
  # Remove whitespace from start of lines and empty lines
  # Not sure why this is necessary, can't seem to fix the xsl to prevent it…
  sed -i -e 's/^[ \t]*//' "${output_folder}/${playlist_file}"
done

echo "All done."
echo "Error count is: $error_count"
exit "$error_count"
